package id.ac.ubaya.informatika.nmp_uas

import android.content.Intent
import android.icu.text.NumberFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_product_details2.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var productsNew:ArrayList<Product> = ArrayList()
    var productsPopular:ArrayList<Product> = ArrayList()
    var v:View?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }



        val queue= Volley.newRequestQueue(activity)
        val url = "http://ubaya.prototipe.net/nmp160718027/productsHome.php"

        val stringReq = StringRequest(
            Request.Method.POST,
            url,
            Response.Listener<String> {
                Log.d("cekproducts",it)
                val obj= JSONObject(it)
                if(obj.getString("result")=="OK"){
                    val dataNew=obj.getJSONArray("dataNew")
                    for(i in 0 until dataNew.length())
                    {
                        val prodObj=dataNew.getJSONObject(i);
                        val product=Product(
                            prodObj.getInt("id"),
                            prodObj.getString("name"),
                            prodObj.getInt("price"),
                            prodObj.getString("description"),
                            prodObj.getString("posted_date"),
                            prodObj.getString("image_url"),
                            prodObj.getInt("stock"),
                            prodObj.getInt("sold"),
                            "",
                            prodObj.getString("shortdesc")
                            )
                        productsNew.add(product)
                    }

                    val dataPopular=obj.getJSONArray("dataPopular")
                    for(i in 0 until dataPopular.length())
                    {
                        val prodObj=dataPopular.getJSONObject(i);
                        val product=Product(
                            prodObj.getInt("id"),
                            prodObj.getString("name"),
                            prodObj.getInt("price"),
                            prodObj.getString("description"),
                            prodObj.getString("posted_date"),
                            prodObj.getString("image_url"),
                            prodObj.getInt("stock"),
                            prodObj.getInt("sold"),
                            "",
                            prodObj.getString("shortdesc")
                            )
                        Log.d("tescheckidpopular", prodObj.getInt("id").toString())

                        productsPopular.add(product)
                    }
                    updateProductNew()
                    updateProductPopular()
                    Log.d("cekgetproductnew",productsNew.toString())
                    Log.d("cekgetproductpop",productsPopular.toString())

                }
            },
            Response.ErrorListener {
                Log.d("cekgetproduct",it.toString())
            }
        )

        queue.add(stringReq)


    }

    fun updateProductNew(){
        val lm = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        var recyclerView=v?.findViewById<RecyclerView>(R.id.productNewView)
        recyclerView?.layoutManager=lm
        recyclerView?.layoutManager=GridLayoutManager(activity,2,  GridLayoutManager.HORIZONTAL, false)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter=ProductAdapter(productsNew,activity!!.applicationContext)
    }

    fun updateProductPopular(){
        val lm = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        var recyclerView=v?.findViewById<RecyclerView>(R.id.productPopularView)
        recyclerView?.layoutManager=lm
        recyclerView?.layoutManager=GridLayoutManager(activity,2,  GridLayoutManager.HORIZONTAL, false)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter=ProductAdapter(productsPopular,activity!!.applicationContext)
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home, container, false)
        return v
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        imgTopup.setImageResource(R.drawable.ic_baseline_payment_24)
        imgUang.setImageResource(R.drawable.ic_baseline_monetization_on_24)

        var buttonTopUp=v!!.findViewById<CardView>(R.id.cardView4)

        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        var uangVirtual = numberFormat.format(Global.userUang).toString()
                txtPayBalance.setText(uangVirtual)

        buttonTopUp?.setOnClickListener{
            val intent = Intent(activity, TopUpActivity::class.java)
            activity!!.startActivity(intent)
        }


    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onStart() {
        super.onStart()
        var buttonTopUp=v!!.findViewById<TextView>(R.id.txtTopUp)
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        var uangVirtual = numberFormat.format(Global.userUang).toString()
        txtPayBalance.setText(uangVirtual)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onResume() {
        super.onResume()
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        var uangVirtual = numberFormat.format(Global.userUang).toString()
        txtPayBalance.setText(uangVirtual)
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}