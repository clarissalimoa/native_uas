package id.ac.ubaya.informatika.nmp_uas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_update_username.*
import org.json.JSONObject

class UpdateUsernameActivity : AppCompatActivity() {
    var users:ArrayList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_username)

        btSavedUsername.setOnClickListener {
            if(!txtUsernameBaru.text.toString().equals("")) {
                val q = Volley.newRequestQueue(this)
                val url = "http://ubaya.prototipe.net/nmp160718027/update_name.php"
                val stringRequest = object : StringRequest(
                    Request.Method.POST,
                    url,
                    Response.Listener {
                        Log.d("updateName", it)
                        // bikin alertDialog messageBox utk data berhasil disimpan.
                        val obj = JSONObject(it)
                        if (obj.getString("result") == "OK") {
                            val builder = AlertDialog.Builder(this)
                            builder.setMessage("Name berhasil diganti")
                            builder.setPositiveButton("OK", null)
                            Global.name = txtUsernameBaru.text.toString()
                            builder.create().show()
                        }

                    },
                    Response.ErrorListener {
                        Log.e("updateName", it.toString())
                    }
                ) {
                    override fun getParams(): MutableMap<String, String> {
                        val params = HashMap<String, String>()
                        params["name"] = txtUsernameBaru.text.toString()
                        params["username"] = Global.usernameLogin

                        return params
                    }
                }
                q.add(stringRequest)
            }
            else
            {
                var snackBar = Snackbar.make(it, "Lengkapi seluruh form", Snackbar.LENGTH_LONG).setAction("Action", null)
                snackBar.show()
            }
        }
    }
}