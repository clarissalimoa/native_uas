package id.ac.ubaya.informatika.nmp_uas

import android.content.Context
import android.content.Intent
import android.icu.text.NumberFormat
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_preview_card_layout.view.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ProductAdapter (val products:ArrayList<Product>, val ctx: Context)
    : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    class ProductViewHolder(val v: View) : RecyclerView.ViewHolder(v)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        //masih belum dibenerin
        var v = inflater.inflate(R.layout.product_preview_card_layout, parent,false)
        return ProductViewHolder(v)
    }


   @RequiresApi(Build.VERSION_CODES.N)
   override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val url = products[position].imageUrl
        Picasso.get().load(url).into(holder.v.imgPrevProduct)
        holder.v.txtNamePrev.text = products[position].name
       holder.v.txtShortDescPrev.text = products[position].shortDesc

       //Currency formatter API 24 ke atas
       val localeID =  Locale("in", "ID")
       val numberFormat = NumberFormat.getCurrencyInstance(localeID)
       var price = numberFormat.format(products[position].price).toString()
        holder.v.txtPricePrev.text = price
//        holder.v.txtDescription.text = products[position].description

       holder.v.imgPrevProduct.setOnClickListener{
           //kalau foto di klik pindah ke productdetails
            val intent = Intent(ctx, ProductDetails::class.java)
           intent.putExtra("productid", products[position].id)
           Log.d("ceksendintentid", products[position].id.toString())
//           intent.putExtra("productname", products[position].name)
//           intent.putExtra("productprice", products[position].price)
//           intent.putExtra("productdesc", products[position].description)
//           intent.putExtra("productstock", products[position].stock)
//           intent.putExtra("productsold", products[position].sold)
//           intent.putExtra("productimg", products[position].imageUrl)
           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
           ctx.startActivity(intent)
       }

        holder.v.btnAddToCartPrev.setOnClickListener{
            var q = Volley.newRequestQueue(ctx)
            //val url = "http://192.168.0.103:8080/nativuas/addcart.php"
            val url = "http://ubaya.prototipe.net/nmp160718027/addtocart.php"

            var stringRequest = object: StringRequest(
                Request.Method.POST, url,
                Response.Listener {
                    Log.d("cekaddtocarthome", it)
                    val obj= JSONObject(it)
                    if(obj.getString("result")=="OK") {
                        Toast.makeText(ctx, obj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener {
                    Log.d("cekaddtocarthome", it.message.toString())
                }
            )
            {
                override fun getParams(): MutableMap<String, String> {
                    var params =  HashMap<String,String>()
                    params.put("idproduk", products[position].id.toString())
                    params.put("price", products[position].price.toString())
                    params.put("username", Global.usernameLogin)

                    return params
                }
            }
            q.add(stringRequest)
        }
    }


    override fun getItemCount(): Int {
        return products.size
    }
}