package id.ac.ubaya.informatika.nmp_uas

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_profile.*
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    val REQUEST_CHANGE_USERNAME = 1
    val REQUEST_CHANGE_PASSWORD = 2

    var user:ArrayList<User> = ArrayList()
    var v: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btUpdateProfile.setOnClickListener {
            val intent = Intent(activity,UpdateUsernameActivity::class.java)
            startActivityForResult(intent, REQUEST_CHANGE_USERNAME)
        }
        btChangePassword.setOnClickListener {
            val intent = Intent(activity,UpdatePasswordActivity::class.java)
            startActivityForResult(intent, REQUEST_CHANGE_PASSWORD)
        }
        textNameProfile.text = Global.name
        txtUsernameView.text = Global.usernameLogin
        txtEmailView.text = Global.email
        txtUangVirtual.text = Global.userUang.toString()

        btLogout.setOnClickListener {
            val intent = Intent(activity,LoginActivity::class.java)
            startActivity(intent)

            Global.usernameLogin = ""
            Global.email = ""
            Global.total = 0
            Global.userUang = 0
        }
    }

    override fun onResume() {
        super.onResume()

        btUpdateProfile.setOnClickListener {
            val intent = Intent(activity,UpdateUsernameActivity::class.java)
            startActivityForResult(intent, REQUEST_CHANGE_USERNAME)
        }
        btChangePassword.setOnClickListener {
            val intent = Intent(activity,UpdatePasswordActivity::class.java)
            startActivityForResult(intent, REQUEST_CHANGE_PASSWORD)
        }

        txtUsernameView.text = Global.usernameLogin
        txtEmailView.text = Global.email
        txtUangVirtual.text = Global.userUang.toString()
        textNameProfile.text = Global.name

        btLogout.setOnClickListener {
            val intent = Intent(activity,LoginActivity::class.java)
            startActivity(intent)

            Global.usernameLogin = ""
            Global.email = ""
            Global.total = 0
            Global.userUang = 0
        }

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_profile, container, false)
        return v;
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}