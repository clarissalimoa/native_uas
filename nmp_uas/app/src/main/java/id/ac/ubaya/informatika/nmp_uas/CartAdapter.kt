package id.ac.ubaya.informatika.nmp_uas

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cart_card_layout.view.*
import kotlinx.android.synthetic.main.fragment_cart.view.*

class CartAdapter(var carts:ArrayList<Cart>,val ctx:Context):RecyclerView.Adapter<CartAdapter.CartViewHolder>() {
    class CartViewHolder(val v:View):RecyclerView.ViewHolder(v){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val inflater=LayoutInflater.from(parent.context)
        var v=inflater.inflate(R.layout.cart_card_layout,parent,false)
        return CartViewHolder(v);
    }

    override fun getItemCount(): Int {
        return carts.size
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val url = carts[position].img_url
        Picasso.get().load(url).into(holder.v.imageViewCart)
        holder.v.textJumlahCart.text = carts[position].quantity.toString()
        holder.v.textNamaProdukCart.text=carts[position].namaProduct
        holder.v.textJumlahHargaCart.text=carts[position].subtotal.toString()

        holder.v.btPlusCart.setOnClickListener {
                val q = Volley.newRequestQueue(ctx)
                val url1 = "http://ubaya.prototipe.net/nmp160718027/setquantitycart.php"
                val stringRequest= object:StringRequest(
                    Request.Method.POST,url1,
                    Response.Listener {
                        Log.d("setqty",it)
                        carts[position].quantity++
                        var newqty = carts[position].quantity
                        var newsubtotal = carts[position].quantity * carts[position].price
                        holder.v.textJumlahCart.text="$newqty"
                        holder.v.textJumlahHargaCart.text="$newsubtotal"
//                        holder.v.btCheckOut.text="CHECKOUT $total"
                    },
                    Response.ErrorListener {
                        Log.d("setqty",it.toString())
                    }
                ){
                    override fun getParams(): MutableMap<String, String> {
                        val params = HashMap<String,String>()
                        params["id"]=carts[position].idproduct.toString()
                        params["ket"]="plus"
                        params["username"]=Global.usernameLogin
                        return params
                    }
                }
                q.add(stringRequest)
        }

        holder.v.btMinCart.setOnClickListener {
            if(carts[position].quantity>1)//min qty 1 gabisa di min sampai 0 (kalau mau delete)
            {
                val q = Volley.newRequestQueue(ctx)
                val url1 = "http://ubaya.prototipe.net/nmp160718027/setquantitycart.php"
                val stringRequest= object:StringRequest(
                    Request.Method.POST,url1,
                    Response.Listener {
                        Log.d("setqty",it)
                        carts[position].quantity--
                        var newqty = carts[position].quantity
                        var newsubtotal = carts[position].quantity * carts[position].price
                        holder.v.textJumlahCart.text="$newqty"
                        holder.v.textJumlahHargaCart.text="$newsubtotal"
                    },
                    Response.ErrorListener {
                        Log.d("setqty",it.toString())
                    }
                ){
                    override fun getParams(): MutableMap<String, String> {
                        val params = HashMap<String,String>()
                        params["id"]=carts[position].idproduct.toString()
                        params["ket"]="min"
                        params["username"]=Global.usernameLogin
                        return params
                    }
                }
                q.add(stringRequest)
            }
        }

        holder.v.btDeleteCart.setOnClickListener {
            val q = Volley.newRequestQueue(ctx)
            val url2 = "http://ubaya.prototipe.net/nmp160718027/deletecart.php"
            val stringRequest= object:StringRequest(
                Request.Method.POST,url2,
                Response.Listener {
                    Log.d("deletecart",it)
                    carts.removeAt(position)
                    notifyDataSetChanged()//refresh

                },
                Response.ErrorListener {
                    Log.d("deletecart",it.toString())
                }
            ){
                override fun getParams(): MutableMap<String, String> {
                    val params = HashMap<String,String>()
                    params["id"]=carts[position].idproduct.toString()
                    params["username"]=Global.usernameLogin
                    return params
                }
            }
            q.add(stringRequest)
        }
    }


}