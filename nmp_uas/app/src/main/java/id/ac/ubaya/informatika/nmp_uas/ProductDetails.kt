package id.ac.ubaya.informatika.nmp_uas

import android.icu.text.NumberFormat
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_details2.*
import kotlinx.android.synthetic.main.product_preview_card_layout.view.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class ProductDetails : AppCompatActivity() {
    var productDetails:Product=Product(0,"",0,"","","",0,0)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details2)

        var productid = intent.getIntExtra("productid",0)
        productDetails = Product(productid,"",0,"","","",0,0)
        //seharusnya cuma ambil id terus volley minta data di bawah
//        var productname = intent.getStringExtra("productname").toString()
//        var productdesc = intent.getStringExtra("productdesc").toString()
//        var imgUrl = intent.getStringExtra("productimg").toString()
//        var productsold = intent.getIntExtra("productsold",0)
//        var productstock = intent.getIntExtra("productstock",0)
//        productprice = intent.getIntExtra("productprice",0)
//        Log.d("cekidfromintent", productid.toString())
//        Log.d("cekpricefromintent", productprice.toString())


        val queue= Volley.newRequestQueue(this)
        val url = "http://ubaya.prototipe.net/nmp160718027/getproductdetails.php"

        val stringReqNew = @RequiresApi(Build.VERSION_CODES.N)
        object: StringRequest(
            Request.Method.POST,
            url,
            Response.Listener<String> {
                Log.d("cekproductsdetails",it)
                val obj= JSONObject(it)
                if(obj.getString("result")=="OK"){
                    val data=obj.getJSONArray("data")
                    for(i in 0 until data.length())
                    {
                        val prodObj=data.getJSONObject(i);
                        val product=Product(
                            prodObj.getInt("id"),
                            prodObj.getString("name"),
                            prodObj.getInt("price"),
                            prodObj.getString("description"),
                            prodObj.getString("posted_date"),
                            prodObj.getString("image_url"),
                            prodObj.getInt("stock"),
                            prodObj.getInt("sold"),
                            prodObj.getString("categoryname")

                        )
                        productDetails = product
                    }
                    Log.d("cekgetproductdetails",productDetails.toString())

                    val localeID =  Locale("in", "ID")
                    val numberFormat = NumberFormat.getCurrencyInstance(localeID)
                    var price = numberFormat.format(productDetails.price).toString()
                    txtPriceDetail.text = price

                    Picasso.get().load(productDetails.imageUrl).into(imgProductDetail)
                    txtNameDetail.text = productDetails.name
                    txtDescriptionDetail.text = productDetails.description
                    txtSoldDetail.setText(productDetails.sold.toString() + " Terjual")
                    txtStockDetail.setText("Stock tersedia: "+productDetails.stock.toString())
                    txtKategoriDetail.setText(productDetails.categoryName)


                }
            },
            Response.ErrorListener {
                Log.d("cekgetproduct",it.toString())
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                var params =  HashMap<String,String>()
                params.put("idproduk", productid.toString())
                return params
            }
        }

        queue.add(stringReqNew)



    }

    override fun onStart() {
        super.onStart()

        btnAddToCartDetail.setOnClickListener{
            var q = Volley.newRequestQueue(this)
            val url = "http://ubaya.prototipe.net/nmp160718027/addtocart.php"
            var stringRequest = object: StringRequest(
                Request.Method.POST,
                url,
                Response.Listener {
                    Log.d("cekproductdetails", it)
                    val obj= JSONObject(it)
                    if(obj.getString("result")=="OK") {
                        Toast.makeText(this, obj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                    },
                Response.ErrorListener {
                    Log.d("cekproductdetailserror", it.toString())
                }
            ) {
                override fun getParams(): MutableMap<String, String> {
                    var params =  HashMap<String,String>()
                    params.put("idproduk", productDetails.id.toString())
                    params.put("price", productDetails.price.toString())
                    params.put("username", Global.usernameLogin)
                    return params
                }
            }
            q.add(stringRequest)
        }

        fabBack.setOnClickListener{
            finish()
        }

    }
}