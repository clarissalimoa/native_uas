package id.ac.ubaya.informatika.nmp_uas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btRegist
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    var users:ArrayList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btLogin.setOnClickListener {
            if(!txtEmailLogin.text.toString().equals("") || !txtPasswordLogin.text.toString().equals("")) {
                val q = Volley.newRequestQueue(this)
                val url = "http://ubaya.prototipe.net/nmp160718027/login.php"
                val stringRequest = object : StringRequest(
                    Request.Method.POST,
                    url,
                    Response.Listener {
                        Log.d("login", it)

                        val obj = JSONObject(it)
                        if (obj.getString("result") == "OK") {
                            val data = obj.getJSONArray("data")
                            for (i in 0 until data.length()) {
                                val playObj = data.getJSONObject(i)
                                val userList = User(
                                    playObj.getString("username"),
                                    playObj.getString("password"),
                                    playObj.getString("name"),
                                    playObj.getString("email"),
                                    playObj.getInt("uang_virtual")
                                )
                                users.add(userList)
                            }
                            Global.usernameLogin = users[0].username
                            Global.email = users[0].email
                            Global.userUang = users[0].uang_virtual
                            Global.name=users[0].name
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        }
                        else{
                            var snackBar = Snackbar.make(btLogin.rootView, "Gagal Login", Snackbar.LENGTH_LONG).setAction("Action", null)
                            snackBar.show()
//                            val builder = AlertDialog.Builder(this)
//                            builder.setMessage("Login gagal")
//                            builder.setPositiveButton("OK", null)
//
//                            builder.create().show()
                        }
                    },
                    Response.ErrorListener {
                        Log.e("login", it.toString())
                    }
                ) {
                    override fun getParams(): MutableMap<String, String> {
                        val params = HashMap<String, String>()
                        params["email"] = txtEmailLogin.text.toString()
                        params["password"] = txtPasswordLogin.text.toString()

                        return params
                    }
                }
                q.add(stringRequest)
            }
            else{
                var snackBar = Snackbar.make(it, "Lengkapi seluruh form", Snackbar.LENGTH_LONG).setAction("Action", null)
                snackBar.show()
            }
        }

        btRegist.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }
}