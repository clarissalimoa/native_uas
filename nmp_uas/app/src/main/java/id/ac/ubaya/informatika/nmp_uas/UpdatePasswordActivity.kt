package id.ac.ubaya.informatika.nmp_uas

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_update_password.*
import kotlinx.android.synthetic.main.activity_update_username.*
import kotlinx.android.synthetic.main.fragment_profile.*
import org.json.JSONObject

class UpdatePasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_password)

        btSaved.setOnClickListener {
            if(!txtPasswordLama.text.toString().equals("") || !txtPasswordBaru.text.toString().equals("") || !txtUlangPasswordBaru.text.toString().equals("")) {
               if(txtPasswordBaru.text.toString().equals(txtUlangPasswordBaru.text.toString())) {
                   val q = Volley.newRequestQueue(this)
                   val url = "http://ubaya.prototipe.net/nmp160718027/update_password1.php"
                   val stringRequest = object : StringRequest(
                       Request.Method.POST,
                       url,
                       Response.Listener {
                           Log.d("update", it)
                           // bikin alertDialog messageBox utk data berhasil disimpan.
                           val obj = JSONObject(it)
                           if (obj.getString("result") == "OK") {
                               val builder = AlertDialog.Builder(this)
                               builder.setMessage("Password berhasil diganti")
                               builder.setPositiveButton("OK", null)

                               builder.create().show()
                           }
                           else{
                               val builder = AlertDialog.Builder(this)
                               builder.setMessage("Password Lama Salah")
                               builder.setPositiveButton("OK", null)

                               builder.create().show()
                           }

                       },
                       Response.ErrorListener {
                           Log.e("update", it.toString())
                       }
                   ) {
                       override fun getParams(): MutableMap<String, String> {
                           val params = HashMap<String, String>()
                           params["username"] = Global.usernameLogin
                           params["passwordLama"] = txtPasswordLama.text.toString()
                           params["passwordBaru"] = txtPasswordBaru.text.toString()
                           params["passwordUlang"] = txtUlangPasswordBaru.text.toString()

                           return params
                       }
                   }
                   q.add(stringRequest)
               }
                else{
                   var snackBar = Snackbar.make(it, "Password baru tidak sesuai dengan ulang password baru!", Snackbar.LENGTH_LONG).setAction("Action", null)
                   snackBar.show()
               }
            }
            else
            {
                var snackBar = Snackbar.make(it, "Lengkapi seluruh form", Snackbar.LENGTH_LONG).setAction("Action", null)
                snackBar.show()
            }
        }
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)


    }
}