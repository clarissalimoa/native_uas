package id.ac.ubaya.informatika.nmp_uas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONObject

class SignUpActivity : AppCompatActivity() {
    var users:ArrayList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        btRegist.setOnClickListener {
            if(!txtUsernameSignUp.text.toString().equals("") || !txtPasswordSignUp.text.toString().equals("") || !txtEmailSignUp.text.toString().equals("")|| !txtUserSignUp.text.toString().equals("")) {
                if (txtPasswordSignUp.text.toString() == txtUlangPasswordSignUp.text.toString()) {
                    val q = Volley.newRequestQueue(this)
                    val url = "http://ubaya.prototipe.net/nmp160718027/regis1.php"
                    val stringRequest = object : StringRequest(
                        Request.Method.POST,
                        url,
                        Response.Listener {
                            Log.d("regist", it)
                            val obj = JSONObject(it)

                            if (obj.getString("result") == "Error") {
                                val builder = AlertDialog.Builder(this)
                                builder.setMessage("Email yang anda masukkan telah ada")
                                builder.setPositiveButton("OK", null)

                                builder.create().show()
                            }
                            else {

                                Global.usernameLogin = txtUsernameSignUp.text.toString()
                                Global.email = txtEmailSignUp.text.toString()
                                Global.userUang = 5000
                                Global.name = txtUserSignUp.text.toString()
                                val intent = Intent(this, MainActivity::class.java)
                                startActivity(intent)
                            }
                        },
                        Response.ErrorListener {
                            Log.e("regist", it.toString())
                        }
                    ) {
                        override fun getParams(): MutableMap<String, String> {
                            val params = HashMap<String, String>()
                            params["username"] = txtUsernameSignUp.text.toString()
                            params["email"] = txtEmailSignUp.text.toString()
                            params["password"] = txtPasswordSignUp.text.toString()
                            params["name"] = txtUserSignUp.text.toString()

                            return params
                        }
                    }
                    q.add(stringRequest)
                }
            }
            else{
                var snackBar = Snackbar.make(it, "Lengkapi seluruh form", Snackbar.LENGTH_LONG).setAction("Action", null)
                snackBar.show()
            }
        }
    }
}