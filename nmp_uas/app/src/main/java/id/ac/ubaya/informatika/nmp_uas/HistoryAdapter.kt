package id.ac.ubaya.informatika.nmp_uas

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.history_card_layout.view.*

class HistoryAdapter(var histories:ArrayList<History>,val ctx:Context):RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {
    class HistoryViewHolder (val v:View):RecyclerView.ViewHolder(v){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val inflater= LayoutInflater.from(parent.context)
        var v=inflater.inflate(R.layout.history_card_layout,parent,false)
        return HistoryAdapter.HistoryViewHolder(v);
    }

    override fun getItemCount(): Int {
      return histories.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
//        val url = histories[position].img_url
//        Picasso.get().load(url).into(holder.v.imageViewHistory)
        holder.v.textQtyHistory.text ="Total quantity: "+ histories[position].quantity.toString()
        holder.v.textJumlahItemHistory.text="Total jenis item: "+histories[position].jumlahitem
        holder.v.textJumlahUangHistory.text=histories[position].total.toString()
        holder.v.textIdHist.text="Order ID: "+histories[position].id.toString()
        holder.v.textTransactionDate.text=histories[position].tgl
    }
}