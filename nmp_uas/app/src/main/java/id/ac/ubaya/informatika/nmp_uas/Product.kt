package id.ac.ubaya.informatika.nmp_uas

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

data class Product(val id:Int, val name:String,
                   val price:Int, val description:String, val posted_date: String,
                   val imageUrl:String, var sold:Int, var stock:Int, var categoryName:String="", var shortDesc:String="")
