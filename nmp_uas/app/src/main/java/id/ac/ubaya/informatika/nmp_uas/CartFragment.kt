package id.ac.ubaya.informatika.nmp_uas

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_cart.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CartFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var carts:ArrayList<Cart> = ArrayList()
    var v:View?=null
    var total=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        val queue=Volley.newRequestQueue(activity)
        val url = "http://ubaya.prototipe.net/nmp160718027/getcart.php"
        val stringReq = object :StringRequest(
            Request.Method.POST,
            url,
            Response.Listener<String> {
                Log.d("cekcarts",it)
                val obj=JSONObject(it)
                if(obj.getString("result")=="OK"){
                    val data=obj.getJSONArray("data")
                    for(i in 0 until data.length())
                    {
                        val cartObj=data.getJSONObject(i);
                        val cart=Cart(
                            cartObj.getInt("idproduct"),
                            cartObj.getString("name"),
                            cartObj.getString("image_url"),
                            cartObj.getInt("quantity"),
                            cartObj.getInt("price"),
                            cartObj.getInt("subtotal")
                        )
                        carts.add(cart)
                    }
                    btCheckOut.isEnabled=true
                    updateList()
                    Log.d("cekcarts",carts.toString())

                }
                else
                {
                    btCheckOut.isEnabled=false
                }
            },
            Response.ErrorListener {
                Log.d("cekcarts",it.toString())

            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val params = HashMap<String,String>()
                params.put("username",Global.usernameLogin)
                return params;
            }
        }
        queue.add(stringReq)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_cart, container, false)
        return v;

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btCheckOut.setOnClickListener {

            val queue=Volley.newRequestQueue(activity)
            val url = "http://ubaya.prototipe.net/nmp160718027/gettotalcart.php"
            val stringReq = object : StringRequest(
                Request.Method.POST,
                url,
                Response.Listener {
                    Log.d("cektotal",it)
                    val obj=JSONObject(it)
                    if(obj.getString("result")=="OK"){
                        val data=obj.getJSONArray("data")
                        for(i in 0 until data.length())
                        {
                            val totObj=data.getJSONObject(i);
                            total=totObj.getInt("total")
                            Global.total=total
                        }

                        AlertDialog.Builder(this.context).apply {
                            setMessage("Checkout sejumlah Rp.$total")
                            setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
                                if(Global.userUang<total){//jika saldo tidak cukup
                                    AlertDialog.Builder(this.context).apply {
                                        setMessage("Saldo anda tidak mencukupi, silahkan Top Up terlebih dahulu")
                                        setPositiveButton("OK",null)
                                        create().show()
                                    }
                                }
                                else{
                                    AlertDialog.Builder(this.context).apply {
                                        //Add history + kurangi saldo + delete cart + refresh
                                        val q = Volley.newRequestQueue(activity)
                                        val url = "http://ubaya.prototipe.net/nmp160718027/processcheckout.php"
                                        val stringReq = object : StringRequest(
                                            Request.Method.POST,
                                            url,
                                            Response.Listener {
                                                Log.d("afterco",it)
                                            //updateduid
                                                Global.userUang=Global.userUang-Global.total
                                            },
                                            Response.ErrorListener {
                                                Log.d("afterco",it.toString())
                                            }
                                        ){
                                            override fun getParams(): MutableMap<String, String> {

                                                val params = HashMap<String,String>()
                                                params.put("username",Global.usernameLogin)
                                                params.put("total",Global.total.toString());
                                                return params;
                                            }
                                        }
                                        q.add(stringReq)

                                        setMessage("Berhasil dipesan! Barang akan dikirim")
                                        setPositiveButton("OK",null)
                                        create().show()
                                        carts.clear()
                                        updateList()
                                        btCheckOut.isEnabled = false
                                    }
                                }
                            })
                            setNegativeButton("Batal",null)
                            create().show()
                        }
                    }
                },
                Response.ErrorListener {
                    Log.d("cektotal",it.toString())
                }
            ){
                override fun getParams(): MutableMap<String, String> {
                    val params = HashMap<String,String>()
                    params.put("username",Global.usernameLogin)
                    return params;
                }
            }
            queue.add(stringReq)

        }
    }


    override fun onResume() {
        super.onResume()
        carts.clear()
        val queue=Volley.newRequestQueue(activity)
        val url = "http://ubaya.prototipe.net/nmp160718027/getcart.php"
        val stringReq = object :StringRequest(
            Request.Method.POST,
            url,
            Response.Listener<String> {
                Log.d("cekcarts",it)
                val obj=JSONObject(it)
                if(obj.getString("result")=="OK"){
                    val data=obj.getJSONArray("data")
                    for(i in 0 until data.length())
                    {
                        val cartObj=data.getJSONObject(i);
                        val cart=Cart(
                            cartObj.getInt("idproduct"),
                            cartObj.getString("name"),
                            cartObj.getString("image_url"),
                            cartObj.getInt("quantity"),
                            cartObj.getInt("price"),
                            cartObj.getInt("subtotal")
                        )
                        carts.add(cart)
                    }
                    updateList()
                    Log.d("cekcarts",carts.toString())
                    btCheckOut.isEnabled=true
                }
                else
                {
                    btCheckOut.isEnabled=false
                }
            },
            Response.ErrorListener {
                Log.d("cekcarts",it.toString())
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val params = HashMap<String,String>()
                params.put("username",Global.usernameLogin)
                return params;
            }
        }
        queue.add(stringReq)
    }

    fun updateList(){
        val lm = LinearLayoutManager(activity)
        var recyclerView=v?.findViewById<RecyclerView>(R.id.cartView)
        recyclerView?.layoutManager=lm
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter=CartAdapter(carts,activity!!.applicationContext)

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CartFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CartFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}