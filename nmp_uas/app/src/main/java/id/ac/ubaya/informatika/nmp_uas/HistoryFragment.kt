package id.ac.ubaya.informatika.nmp_uas

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HistoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HistoryFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var histories:ArrayList<History> = ArrayList()
    var v:View?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        val queue= Volley.newRequestQueue(activity)
        val url = "http://ubaya.prototipe.net/nmp160718027/gethistory.php"
        val stringReq = object : StringRequest(
            Request.Method.POST,
            url,
            Response.Listener<String> {
                Log.d("cekhists",it)
                val obj= JSONObject(it)
                if(obj.getString("result")=="OK"){
                    val data=obj.getJSONArray("data")
                    for(i in 0 until data.length())
                    {
                        val hisObj=data.getJSONObject(i);
                        val history=History(
                            hisObj.getInt("id"),
                            hisObj.getInt("jumlahItem"),
                            hisObj.getInt("quantity"),
                            hisObj.getInt("total"),
                            hisObj.getString("transaction_date")
                        )
                        histories.add(history)
                    }
                    updateList()
                    Log.d("cekhists",histories.toString())
                }
            },
            Response.ErrorListener {
                Log.d("cekhists",it.toString())
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val params = HashMap<String,String>()
                params.put("username",Global.usernameLogin)
                return params;
            }
        }
        queue.add(stringReq)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_history, container, false)
        return v;
    }

    override fun onResume() {
        super.onResume()
        histories.clear()
        val queue= Volley.newRequestQueue(activity)
        val url = "http://ubaya.prototipe.net/nmp160718027/gethistory.php"
        val stringReq = object : StringRequest(
            Request.Method.POST,
            url,
            Response.Listener<String> {
                Log.d("cekhists",it)
                val obj= JSONObject(it)
                if(obj.getString("result")=="OK"){
                    val data=obj.getJSONArray("data")
                    for(i in 0 until data.length())
                    {
                        val hisObj=data.getJSONObject(i);
                        val history=History(
                            hisObj.getInt("id"),
                            hisObj.getInt("jumlahItem"),
                            hisObj.getInt("quantity"),
                            hisObj.getInt("total"),
                            hisObj.getString("transaction_date")
                        )
                        histories.add(history)
                    }
                    updateList()
                    Log.d("cekhists",histories.toString())
                }
            },
            Response.ErrorListener {
                Log.d("cekhists",it.toString())
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val params = HashMap<String,String>()
                params.put("username",Global.usernameLogin)
                return params;
            }
        }
        queue.add(stringReq)
    }

    fun updateList(){
        val lm = LinearLayoutManager(activity)
        var recyclerView=v?.findViewById<RecyclerView>(R.id.historyView)
        recyclerView?.layoutManager=lm
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter=HistoryAdapter(histories,activity!!.applicationContext)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HistoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HistoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}