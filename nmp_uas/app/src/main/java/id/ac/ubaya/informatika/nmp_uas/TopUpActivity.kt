package id.ac.ubaya.informatika.nmp_uas

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_top_up.*
import org.json.JSONArray
import org.json.JSONObject

class TopUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up)
        var arrUser:ArrayList<User>;

        btTopUp.setOnClickListener {
            val q = Volley.newRequestQueue(this)
            val url = "http://ubaya.prototipe.net/nmp160718027/topup.php"
            val stringReq = object : StringRequest(
                Request.Method.POST,
                url,
                Response.Listener {
                    Log.d("cektopup",it)
                    Global.userUang =  Global.userUang + tbTopUp.text.toString().toInt()
                    //alert berhasil
                    AlertDialog.Builder(this).apply {
                        setMessage("Berhasil mengisi saldo "+tbTopUp.text.toString())
                        setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
                            tbTopUp.setText("")
                        })
                        create().show()
                    }
                },
                Response.ErrorListener {
                    Log.d("cektopup",it.toString())
                }
            ){
                override fun getParams(): MutableMap<String, String> {
                    val params = HashMap<String,String>()
                    params.put("username",Global.usernameLogin)
                    params.put("jumlah",tbTopUp.text.toString());
                    return params;
                }
            }
            q.add(stringReq)
        }
    }
}