-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2020 at 11:05 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nativ_uas`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE `cart_details` (
  `username` varchar(45) NOT NULL,
  `idproduct` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `is_selected` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart_details`
--

INSERT INTO `cart_details` (`username`, `idproduct`, `quantity`, `subtotal`, `is_selected`) VALUES
('cla', 1, 2, 178000, 1),
('sann', 2, 1, 75000, 1),
('sann', 4, 2, 150000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Make up'),
(2, 'Skin Care'),
(3, 'Body Care');

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `invoice_number` varchar(45) DEFAULT NULL,
  `transaction_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `is_paid` tinyint(4) DEFAULT '0',
  `promo_code` varchar(45) DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `delivered_date` datetime DEFAULT NULL,
  `arrived_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `histories`
--

INSERT INTO `histories` (`id`, `username`, `invoice_number`, `transaction_date`, `total`, `subtotal`, `discount`, `is_paid`, `promo_code`, `paid_date`, `delivered_date`, `arrived_date`) VALUES
(1, 'sann', NULL, '2020-11-14 09:57:29', 225000, 225000, NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history_details`
--

CREATE TABLE `history_details` (
  `idproduct` int(11) NOT NULL,
  `idhistories` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `rating` varchar(200) DEFAULT NULL,
  `star` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history_details`
--

INSERT INTO `history_details` (`idproduct`, `idhistories`, `quantity`, `subtotal`, `rating`, `star`) VALUES
(2, 1, 1, 75000, NULL, NULL),
(4, 1, 2, 150000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `posted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_url` varchar(100) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `rating` double DEFAULT NULL,
  `sold` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `posted_date`, `image_url`, `categories_id`, `rating`, `sold`, `stock`) VALUES
(1, 'Make Over Blush On', 89000, 'Product blush on terbaru dari make over', '2020-11-14 09:52:05', 'https://s3-ap-southeast-1.amazonaws.com/img-sociolla/img/p/1/7/7/1/7/17717-large_default.jpg', 1, NULL, NULL, NULL),
(2, 'Scarlett Whitening Scrub', 75000, 'Scrub mencerahkan dan mengangkat sel kulit mati', '2020-11-14 09:52:05', 'https://p.ipricegroup.com/uploaded_d03a2d68c0cb54657b2b1945f309962d.jpg', 3, NULL, NULL, NULL),
(3, 'Maybelline fit me', 120000, 'Dapatkan hasil matte & full coverage tahan lama hingga 24jam namun terasa ringan di wajah. Coverage yang dapat meratkan kulit & wajah.', '2020-11-14 09:55:45', 'https://id-live-01.slatic.net/p/0a7e41890ac35a0f467195da7041ed63.jpg', 1, NULL, NULL, NULL),
(4, 'Scarlett Whitening Serum', 75000, 'Mencerahkan wajah hanya dengan 75k saja', '2020-11-14 09:52:05', 'https://id-test-11.slatic.net/p/63b6ab7b50799d00621e5749cb1550e1.jpg', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `categories_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(45) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(45) NOT NULL,
  `uang_virtual` int(11) DEFAULT '5000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `salt`, `name`, `email`, `uang_virtual`) VALUES
('cla', '123', NULL, 'Clarissa', 'clamoa@gmail.com', 5000),
('ncy', '123', NULL, 'Nancy', 'ncy@gmail.com', 5000),
('sann', '123', NULL, 'Santi', 'sann@gmail.com', 500000);

-- --------------------------------------------------------

--
-- Table structure for table `variations`
--

CREATE TABLE `variations` (
  `id` int(11) NOT NULL,
  `idproduct` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD PRIMARY KEY (`username`,`idproduct`),
  ADD KEY `fk_users_has_products_products1_idx` (`idproduct`),
  ADD KEY `fk_users_has_products_users1_idx` (`username`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_histories_users1_idx` (`username`);

--
-- Indexes for table `history_details`
--
ALTER TABLE `history_details`
  ADD PRIMARY KEY (`idproduct`,`idhistories`),
  ADD KEY `fk_histories_has_products_products1_idx` (`idproduct`),
  ADD KEY `fk_history_details_histories1_idx` (`idhistories`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_products_categories1_idx` (`categories_id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subcategories_categories1_idx` (`categories_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `variations`
--
ALTER TABLE `variations`
  ADD PRIMARY KEY (`id`,`idproduct`),
  ADD KEY `fk_variations_products1_idx` (`idproduct`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `variations`
--
ALTER TABLE `variations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD CONSTRAINT `fk_users_has_products_products1` FOREIGN KEY (`idproduct`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_products_users1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `fk_histories_users1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_details`
--
ALTER TABLE `history_details`
  ADD CONSTRAINT `fk_histories_has_products_products1` FOREIGN KEY (`idproduct`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_details_histories1` FOREIGN KEY (`idhistories`) REFERENCES `histories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `fk_subcategories_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `variations`
--
ALTER TABLE `variations`
  ADD CONSTRAINT `fk_variations_products1` FOREIGN KEY (`idproduct`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
