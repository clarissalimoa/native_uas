<?php
error_reporting(E_ERROR | E_PARSE);
$c = new mysqli("localhost", "nmp160718027", "ubaya", "nmp160718027");
if($c->connect_errno) 
{
    echo json_encode(array('result'=> 'ERROR', 'message' => 'Failed to connect DB'));
    die();
}

if(isset($_POST['username'])){
   $username = $_POST['username'];
   $total = $_POST['total'];
   $subtotal = $_POST['total'];


   

   try {
    //begin transaction, klo ada yg error dirollback semua gajadi insert ke database
   $c->begin_transaction();

   //check saldo
   $sql = "select uang_virtual from users where username = ?";
    if ($c->prepare($sql)) {
        $stmt = $c->prepare($sql);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        echo 'done select virtualpay';
    }
    $result = $stmt->get_result(); 

    //kurangin saldo
    if($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        if((int)$row['uang_virtual'] >= (int)$total){
            // echo (int)$row['uang_virtual'].'>='.(int)$total.' uang cukup';
            $sql = "UPDATE users SET uang_virtual=uang_virtual-$total where username='$username'";
            $c->query($sql);
        } 
        else{
            $arr=array("result"=>"ERROR","message"=>"Uang virtual $username kurang");
            die(json_encode($arr)); 
        }
    }
    else{
        $arr=array("result"=>"ERROR","message"=>"Tidak ditemukan uang virtual username $username");
        die(json_encode($arr)); 
    }


   //insert new history
   $sql = "insert into histories(username, total, subtotal) values (?,?,?)";
       if ($c->prepare($sql)) {
           $stmt = $c->prepare($sql);
           $stmt->bind_param('sii', $username,$total, $subtotal);
           $stmt->execute();
       }
    $idhistory = $stmt->insert_id;
    //  echo $idhistory; //done bener

    //get cart
    $sql = "select * from cart_details where username = ?";
    if ($c->prepare($sql)) {
        $stmt = $c->prepare($sql);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        // echo 'done select cart';
    }
    $result = $stmt->get_result(); 
    $stmt->close();
    // echo "numrows=".$result->num_rows;

    $cartdetails = array();
    if($result->num_rows > 0) {
        // echo 'ok numrows>1';
        while ($row = $result->fetch_assoc()) 
        {
            $cartdetails[] = $row;
        //    echo $row["username"];
        }    
        // echo 'while done';
    } 
    

    //add cart to history details
      foreach($cartdetails as $cd){
        //  echo  $cd['idproduct'].' '.$idhistory.' '. $cd['quantity'].' '.$cd['subtotal'].' '.$cd['price'];

        $sql = "insert into history_details(idproduct, idhistories, quantity, subtotal, price) values (?,?,?,?,?)";
        if ($c->prepare($sql)){
            // echo "prepare history ok";
            $stmt = $c->prepare($sql);
            $stmt->bind_param('iiiii', $cd['idproduct'],$idhistory, $cd['quantity'],$cd['subtotal'],$cd['price']);
            $stmt->execute();
        }
        $stmt->close();
      }

   //delete cart where username
   $sql = "delete from cart_details where username = ?";
//    echo "delete";
   if ($c->prepare($sql)) {
    //    echo "if prepare delete";
    $stmt = $c->prepare($sql);
    $stmt->bind_param('s', $username);
    $stmt->execute();
    echo 'done delete cart';
    }

   

    $arr=array("result"=>"OK","message"=>"Berhasil tambah history $username");
    echo json_encode($arr);

    // Kalau gaada error sampe bawah sini lgsg commit
    $c->commit();
    } catch (mysqli_sql_exception $exception){
        $c->rollback();

        throw $exception;
    }
}
else{
    $arr=array("result"=>"ERROR","message"=>"Gagal tambah history");
    die(json_encode($arr));
}
?>
